// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}


let obj = {"first_name": "JoHN", "last_name": "SMith"}


const getFullName = (obj) => {
let objKeys = Object.keys(obj)
let fullName = ""



for (i=0; i < objKeys.length ;  i++){

    let name = objKeys[i]

let titleCasedName = obj[name][0].toUpperCase() + obj[name].slice(1).toLowerCase()

    fullName = fullName +  titleCasedName + " "


}

return fullName
}


module.exports = getFullName