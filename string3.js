

let date = "10/1/2021"

let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


const printMonth = (date) => {
    if (date == undefined) return "Enter valid date"
    if(Object.prototype.toString.call(date) !== "[object String]") return "Not a string"

    let dateArray = date.split("/")

    let month = dateArray[1]

    

    return months[parseInt(month)-1]
}


module.exports = printMonth