// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 


const formatString = (s) => {
    if(s == undefined) return ""
let new_string = ""

for(i=0; i<s.length; i++){
    if(s[i] != "$" && s[i] != ","){
        new_string = new_string + s[i]
    }
}
//console.log(new_string)

if(!isNaN(new_string)){
    return parseFloat(new_string)
}
else {
    return 0
}


}


module.exports = formatString;